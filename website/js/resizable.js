$(document).ready(function(){
	$(function() {
		$("main div img").resizable({
			aspectRatio: 1 / 1,
			minHeight: 150,
			minWidth: 150,			
			maxHeight: 300,
			maxWidth: 300

		});
	});
});